/// <reference types="cypress"/>
import moment from 'moment'



describe('Should test at a functional level', () => {

    //let token
    let id

    before(() => {
        cy.getToken('a@a', 'a')
        //.then(tkn => {
            //        token = tkn
            //})

    })

    beforeEach(() => {
        cy.resetRest()

    })

    it("Should create an count", () => {
        cy.request({
            url: '/contas',
            method: 'POST',
            //headers: { Authorization: `JWT ${token}` },

            body: {
                nome: "Conta test university"
            }

        }).as('response')

        cy.get('@response').then(res => {
            expect(res.status).to.be.equal(201)
            expect(res.body).to.have.property('id')
            expect(res.body).to.have.property('nome', 'Conta test university')
        })

    })


    it("Should update an count", () => {
        cy.request({
            url: '/contas',
            method: 'GET',
            //headers: { Authorization: `JWT ${token}` },
            qs: {
                nome: "Conta para alterar"
            }

        }).then(res => {

            cy.request({
                url: `/contas/${res.body[0].id}`,
                method: 'PUT',
                //headers: { Authorization: `JWT ${token}` },
                body: {
                    nome: "Conta test university alterada via request"
                }
            }).as('response')

            cy.get('@response').then(res => {
                expect(res.status).to.be.equal(200)

            })
        })

    })

    it("Should not create an count with same name", () => {
        cy.request({
            url: '/contas',
            method: 'POST',
            //headers: { Authorization: `JWT ${token}` },

            body: {
                nome: "Conta com movimentacao"
            },
            failOnStatusCode:false
        }).as('response')

        cy.get('@response').then(res => {
            expect(res.status).to.be.equal(400)
            expect(res.body.error).to.be.equals('Já existe uma conta com esse nome!')
        })

    })

    it("Should create a moviment", () => {              
        var dtTransacao = moment().subtract(1, "days").format("DD/MM/YYYY")
        var dtPagamento = moment().format("DD/MM/YYYY")

        cy.getCountByName('Conta para movimentacoes')
        .then(contaId => {
            cy.request({
                url: '/transacoes',
                method: 'POST',
                //headers: { Authorization: `JWT ${token}` },
    
                
                body: {
                    
                        conta_id: contaId,
                        data_pagamento: dtPagamento,
                        data_transacao: dtTransacao,
                        descricao: "moviment1",
                        envolvido: "eu mesmo",
                        status: true,
                        tipo: "REC",
                        valor: "20.22"
                               
                }
                
            }).as('response')
        })      
        cy.get('@response').its('status').should('be.equal',201)
        cy.get('@response').its('body.id').should('exist')
    })

    it("Should delete a moviment", () => {
        cy.request({
            url: '/transacoes',
            method: 'GET',
            //headers: { Authorization: `JWT ${token}` },
            qs: {descricao: "Movimentacao para exclusao"}       
        }).then(res =>{
            cy.request({
                url:`/transacoes/${res.body[0].id}`,
                method: 'DELETE',
                //headers: { Authorization: `JWT ${token}` },
            }).its('status').should('be.equal', 204)
        })
    })

})