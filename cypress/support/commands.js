Cypress.Commands.add('getToken', (user, passwd) => { 
    cy.request({
        method: 'POST',
        url: '/signin',
        body:{
            email:user,
            redirecionador:"false",
            senha:passwd
        }
        
    }).its('body.token').should('not.be.empty')
        .then(token => {
            Cypress.env('token', token)
            return token
        })


})
 
Cypress.Commands.add('resetRest', () => { 
    cy.getToken('a@a','a').then(token => { 
        cy.request({
            method: 'GET',
            url: '/reset',
            headers: {Authorization: `JWT ${token}`}
        }).its('status').should('be.equal',200)
    })



})


Cypress.Commands.add('getCountByName', name => { 
    cy.getToken('a@a','a').then(token => { 
        cy.request({
            method: 'GET',
            url: '/contas',
            headers: {Authorization: `JWT ${token}`},
            qs: {
                nome: name
            }

        }).then(res =>{

            return res.body[0].id
        })
    })

})

Cypress.Commands.overwrite('request', (originalFn, ...Options)=>{
    if(Options.length == 1){
        if(Cypress.env('token')){
            Options[0].headers = {
                Authorization: `JWT ${Cypress.env('token')}`
            }
            
        }
    }
    return originalFn(...Options)
})